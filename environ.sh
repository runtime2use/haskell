#!/bin/bash

export HASKPATH="$LANG_PATH/dist"

################################################################################

ronin_require_haskell () {
    echo hello world >/dev/null
}

ronin_include_haskell () {
    motd_text "    -> Haskell  : "$HASKPATH
}

################################################################################

ronin_setup_haskell () {
    echo hello world >/dev/null
}

